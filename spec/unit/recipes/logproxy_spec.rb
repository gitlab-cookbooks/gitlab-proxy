# frozen_string_literal: true

# Cookbook:: gitlab-proxy::logproxy
# Spec:: default
#
# Copyright:: 2017, GitLab B.V., MIT.

require 'spec_helper'
describe 'gitlab-proxy::logproxy' do
  before do
    allow_any_instance_of(Chef::Recipe).to receive(:get_secrets)
      .with('fake_backend', 'fake_path', 'fake_key')
      .and_return('log-proxy' => { host: 'somehost.com', pass: 'somepass' })
  end

  context 'when all attributes are default, on Ubuntu 16.04' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new(platform: 'ubuntu',
                                 version: '16.04') do |node|
        node.normal['gitlab-proxy'] = {
          'secrets' => {
            'backend' => 'fake_backend',
            'path' => 'fake_path',
            'key' => 'fake_key'
          }
        }
      end.converge(described_recipe)
    end

    it 'converges succesfully' do
      expect { chef_run }.to_not raise_error
    end
  end
end
