# frozen_string_literal: true

# Cookbook Name:: gitlab-proxy::logproxy
# Recipe:: default
#
# Copyright 2016, GitLab Inc.
#
# License: MIT
#
include_recipe 'chef_nginx'

log_proxy_conf = get_secrets(node['gitlab-proxy']['secrets']['backend'],
                             node['gitlab-proxy']['secrets']['path'],
                             node['gitlab-proxy']['secrets']['key'])

template "#{node['nginx']['dir']}/sites-available/log-proxy" do
  source 'log-proxy.erb'
  owner  'root'
  group  'root'
  mode   '0644'
  variables(
    host: log_proxy_conf['log-proxy']['host'],
    auth: log_proxy_conf['log-proxy']['pass']
  )
end

nginx_site 'default' do
  enable false
end

nginx_site 'log-proxy' do
  enable true
  notifies :restart, resources(service: 'nginx'), :delayed
end
