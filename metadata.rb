name             'gitlab-proxy'
maintainer       'GitLab Inc.'
maintainer_email 'ops-contact@gitlab.com'
license          'MIT'
description      'Installs/Configures gitlab-proxy'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '1.0.3'

depends 'gitlab_secrets'
depends 'chef_nginx'
