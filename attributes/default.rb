# frozen_string_literal: true

default['gitlab-proxy']['log-proxy']['host'] = 'http://example.com'
default['gitlab-proxy']['log-proxy']['pass'] = 'XXXXX'
default['gitlab-proxy']['log-proxy']['redirects_enable'] = false
